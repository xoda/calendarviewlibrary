export const resources = [
  {
    resourceId: 2,
    resourceTitle: 'Habeeb Rahman'
  },
  {
    resourceId: 3,
    resourceTitle: 'Rohit kumar'
  },
  {
    resourceId: 4,
    resourceTitle: 'Rohit kumaran'
  }
]


export const events = [
  {
    id: 1,
    title: 'yoga',
    avatarText: 'HR',
    start: '2020-11-19 16:00',
    end: '2020-11-19 17:00',
    background: 'green',
    resourceId: 2
  },
  {
    id: 2,
    title: 'zumba',
    avatarText: 'KR',
    start: '2020-11-19 17:00',
    end: '2020-11-19 18:00',
    resourceId: 2,
    tooltip: 'zumba 11:00 - 12:00\nbookings:\nhabeeb - trial - 9633733367\nrahul - pro - 9633733367'
  },
  {
    id: 3,
    title: 'zumba',
    avatarText: 'KR',
    start: '2020-11-19 19:00',
    end: '2020-11-19 19:15',
    resourceId: 2,
    tooltip: 'zumba 11:00 - 12:00\nbookings:\nhabeeb - trial - 9633733367\nrahul - pro - 9633733367'
  },
  {
    id: 4,
    title: 'zumba',
    avatarText: 'KR',
    start: '2020-11-19 04:15',
    end: '2020-11-19 04:25',
    resourceId: 2,
  },
  {
    id: 5,
    title: 'zumba',
    avatarText: 'KR',
    start: '2020-11-19 05:15',
    end: '2020-11-19 12:30',
    resourceId: 2,
  },
  {
    id: 6,
    title: 'zumbass',
    avatarText: 'KR',
    start: '2020-11-19 18:45',
    end: '2020-11-19 19:00',
    resourceId: 2,
    // tooltip: 'zumba 11:00 - 12:00\nbookings:\nhabeeb - trial - 9633733367\nrahul - pro - 9633733367'
  },
]
