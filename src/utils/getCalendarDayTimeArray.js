import moment from "moment";

export default (min, max) => {
  const hours = Array.from({
      length: 96
    }, (_, hour) => {
      return moment({
        hour: Math.floor(hour / 4),
        minutes: (hour % 4 === 0 ? 0 : (hour % 4 === 1 ? 15 : ( hour % 4 === 2 ? 30: 45 )))
      }).format("HH:mm");
    }
  );
  let timeArray = hours;
  if (min && max) {
    timeArray = hours.filter(el => el >= min && el <= max);
  }
  return timeArray;
}
