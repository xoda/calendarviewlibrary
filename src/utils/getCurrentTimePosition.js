import moment from "moment";

export default (min,max) => {
  let start = min ? moment(min,"HH:mm") : moment().startOf('day')
  let current_time = moment()
  let diff = current_time.diff(start,"minute")
  let style = {
    top: diff +26
  }
  if(moment().format("HH:mm") >= max )
    style.display = "none"
  return style
}
