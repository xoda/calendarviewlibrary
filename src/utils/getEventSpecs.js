import moment from "moment";

const SINGLE_EVENT_HEIGHT = 15;

export default (event, week = false) => {
  let diff = moment(event.end).diff(moment(event.start), "hour", true);
  let minute = moment(event.start).format("mm");
  minute = parseInt(minute);
  // let offset_minute
  // if (minute >= 0 && minute < 30) {
  //   offset_minute = minute
  // } else
  //   offset_minute = minute - 30


  let offset_minute = minute % (week ? 30 : 15);

  // 15 minute  = single event height px
  // 1 minute = single event/15 px height
  let offset = (SINGLE_EVENT_HEIGHT / 15) * offset_minute;
  let event_height = SINGLE_EVENT_HEIGHT * diff * 4;

  let smallEventStyle = {};
  if (diff <= 0.25) {
    smallEventStyle = {
      // fontSize: "95%",
      padding: 0,
      textOverflow: "ellipsis",
      whiteSpace: "nowrap"
    };
  }

  return {
    height: event_height,
    offset,
    smallEventStyle
  };
}
