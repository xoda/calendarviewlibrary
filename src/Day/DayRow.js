import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import styles from "../styles/day_style.module.css";
import Events from "./Events";
import moment from "moment";
import Event from "./Events/Event";
import DayContext from "../Context/DayContext";

const DayRow = props => {
  let {
    time,
    resources,
    dayEvents,
    selectedDate,
    resourceIdAccessor,
    onSelectEvent,
    onSelectSlot,
    replaceEvent,
    sourceEvent,
    setSourceEvent
  } = props;
  let { data, setData } = useContext(DayContext);
  const [entered, setEntered] = useState({});

  const selectSlot = () => {
    let date = selectedDate.format("YYYY-MM-DD");
    let start = date + ` ${time}`;
    let end = date + ` ${moment(time, "HH:mm").add(30, "minute").format("HH:mm")}`;
    let slotInfo = {
      start: moment(start).toDate(),
      end: moment(end).toDate()
    };
    onSelectSlot(slotInfo);
  };
  const onDragEnter = (e, item) => {
    e.preventDefault();
    e.stopPropagation();
    setEnteredBoolean(item, true);
    let target = e.currentTarget;
    setData({
      ...data, dragEnterPosition: {
        top: target.offsetTop,
        left: target.offsetLeft,
        width: target.offsetWidth,
        position: "absolute",
        zIndex: 10
      },
      draggingEvent: getDraggedEventTime(sourceEvent, item)
    });
  };
  const onDragLeave = (e, item) => {
    e.preventDefault();
    e.stopPropagation();
    setEnteredBoolean(item, false);
  };

  const setEnteredBoolean = (item, bool = false) => {
    let count = entered[`${item[resourceIdAccessor]}_${time}`] || 0;
    bool ? count++ : (count > 0 && count--);
    setEntered({
      ...entered,
      [`${item[resourceIdAccessor]}_${time}`]: count
    });
  };

  const onDrop = (e, resource, cancel = false) => {
    setEntered({});
    e.stopPropagation();
    e.preventDefault();
    // let event = e.dataTransfer.getData("event");
    // let source_event = JSON.parse(event);
    let source_event = JSON.parse(JSON.stringify(sourceEvent));
    let target_event = getDraggedEventTime(source_event, resource, cancel);
    replaceEvent(source_event, target_event);
    e.dataTransfer.clearData();
    setData({ ...data, isDragStarted: false,dragEnterPosition: {display: "none"} });
  };

  const onDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const getDraggedEventTime = (source_event, resource, cancel = false) => {
    let Time = (cancel ? moment(data.draggingEvent.start).format("HH:mm") : time);
    let resourceId = cancel ? data.draggingEvent.resourceId : resource[resourceIdAccessor];
    let event_diff = moment(source_event.end).diff(moment(source_event.start), "minute");
    let start_time = moment(source_event.start).format("YYYY-MM-DD ") + Time;
    return {
      ...source_event,
      start: start_time,
      end: moment(start_time).add(event_diff, "minute").format("YYYY-MM-DD HH:mm"),
      resourceId
    };
  };
  let showTime = !["30", "45", "15"].includes(moment(time, "HH:mm").format("mm"));
  let showBorder = ["30", "00", "15"].includes(moment(time, "HH:mm").format("mm"));
  let formattedTime = moment(time,"hh:mm").format("h A")
  return (
    <tr>
      <td className={`${(showBorder ? styles.noBorder : "")} ${styles.time}`}>{showTime ? formattedTime : ""}</td>
      {
        resources ? resources.map((item, index) => {
            let isEntered = entered[`${item[resourceIdAccessor]}_${time}`] > 0;
            return (
              <td
                onDragEnter={(e) => onDragEnter(e, item)}
                onDragLeave={(e) => onDragLeave(e, item)}
                onDrop={(e) => onDrop(e, item)}
                onDragOver={onDragOver}
                onDragEnd={(e) => {
                  e.target.style.zIndex = 2;
                  e.target.style.opacity = 1;
                  onDrop(e, item, true);
                }}
                className={(showBorder ? styles.noBorder : "") + " " + (isEntered ? styles.dropzone : "")}
                onClick={selectSlot} style={{ position: "relative" }} key={index}>
                {/*{*/}
                {/*  isEntered &&*/}
                {/*  <Event item={getDraggedEventTime(sourceEvent,item)} movingEvent={true} />*/}
                {/*}*/}

                <Events
                  resource={item}
                  {...{ dayEvents, selectedDate, time, resourceIdAccessor, onSelectEvent, setSourceEvent, sourceEvent }}
                />
              </td>
            );
          })
          : <td onClick={selectSlot} style={{ position: "relative" }}>
            <Events
              {...{ dayEvents, selectedDate, time, resourceIdAccessor, onSelectEvent, sourceEvent }}
            />
          </td>
      }
    </tr>
  );
};

DayRow.propTypes = {
  time: PropTypes.string,
  resources: PropTypes.array,
  dayEvents: PropTypes.object,
  selectedDate: PropTypes.object,
  resourceIdAccessor: PropTypes.string,
  onSelectEvent: PropTypes.func,
  onSelectSlot: PropTypes.func,
  replaceEvent: PropTypes.func,
  sourceEvent: PropTypes.object, // store source event data while drag
  setSourceEvent: PropTypes.func // store source event data while drag
};

export default DayRow;
