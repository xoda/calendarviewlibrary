import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styles from '../styles/style.module.css'
import getCurrentTimePosition from "../utils/getCurrentTimePosition";

const CurrentTime = props => {
  const {min,max} = props
  const [positionStyle,setPositionStyle] = useState(getCurrentTimePosition(min,max))
  useEffect(() => {
    const timer = setInterval(() => { // Creates an interval which will update the current data every minute
      // This will trigger a rerender every component that uses the useDate hook.
      setPositionStyle(getCurrentTimePosition(min,max))
    }, 1000);
    return () => {
      clearInterval(timer); // Return a funtion to clear the timer so that it will stop being called on unmount
    }
  },[])

  return (
    <div  style={positionStyle} className={styles.currentTime} >
      <div className={styles.dot} ></div>
    </div>
  );
};

CurrentTime.propTypes = {
    min: PropTypes.string,
    max: PropTypes.string,
};

export default CurrentTime;
