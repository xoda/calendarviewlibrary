import React, { useContext } from "react";
import PropTypes from "prop-types";
import styles from "../styles/day_style.module.css";
import getEventSpecs from "../utils/getEventSpecs";
import moment from "moment";
import Event from "./Events/Event";
import DayContext from "../Context/DayContext";

const Events = props => {
  let { time, resource, dayEvents, selectedDate, resourceIdAccessor, onSelectEvent, setSourceEvent } = props;

  let dateTime = selectedDate.format("YYYY-MM-DD") + ` ${time}`;
  let events = dayEvents[dateTime];
  let { data, setData } = useContext(DayContext);

  const onDragStart = (e, item) => {
    let el = e.target.cloneNode();
    el.style.display = "none";
    e.dataTransfer.setDragImage(el, 0, 0);
    e.dataTransfer.setData("event", JSON.stringify(item));
    let target = e.target;
    setTimeout(() => {
      target.style.zIndex = -1
      target.style.opacity = 0.7
    },100)
    setSourceEvent(item);
    setData({ ...data, isDragStarted: true });
  };

  return (
    <div>
      {
        events && events.map((item, index) => {
          if ((resource && resource[resourceIdAccessor] === item[resourceIdAccessor]) || !resource)
            return (
              <Event key={index} {...{ item, onSelectEvent, onDragStart }} />
            );
        })
      }

    </div>
  );
};

Events.propTypes = {
  time: PropTypes.string,
  resource: PropTypes.object,
  dayEvents: PropTypes.object,
  selectedDate: PropTypes.object,
  resourceIdAccessor: PropTypes.string,
  onSelectEvent: PropTypes.func,
  setSourceEvent: PropTypes.func
};

export default Events;
