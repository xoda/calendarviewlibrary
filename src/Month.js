import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import getCalendarMonthArray from "./utils/getCalendarMonthArray";
import WeekRow from "./Month/WeekRow";
import monthStyles from "./styles/month_style.module.css";
import classNames from "classnames";
import getMonthEvents from "./Month/utils/getMonthEvents";
import WeekHeader from "./Month/WeekHeader";
import DraggingEvent from "./Month/DayEvents/DraggingEvent";
import MonthContext from "./Context/MonthContext";

const Month = (props) => {
  const { date, monthRootClassName, events, fixedHeader, onSelectEvent, onSelectSlot, replaceEvent } = props;
  let range = getCalendarMonthArray(date);
  let monthEvents = getMonthEvents(events);

  const [sourceEvent, setSourceEvent] = useState({});
  const { data } = useContext(MonthContext);

  return (
    <div style={{position: "relative"}} className={classNames(monthStyles.monthView, monthRootClassName)}>
      {
        data.isDragStarted &&
        <div className={monthStyles.onDrag}>
          <DraggingEvent item={sourceEvent} movingEvent={true} />
        </div>
      }
      <table>
        <WeekHeader {...{ fixedHeader }} />
        <tbody>
        {
          range && range.map((item, index) =>
            <WeekRow
              {...{
                monthEvents,
                onSelectEvent,
                onSelectSlot,
                date,
                replaceEvent,
                setSourceEvent,
                sourceEvent
              }}
              key={index}
              days={item.days}
            />
          )
        }
        </tbody>
      </table>

    </div>
  );
};

Month.propTypes = {
  date: PropTypes.string,
  events: PropTypes.array,
  monthRootClassName: PropTypes.string,
  fixedHeader: PropTypes.bool,
  onSelectEvent: PropTypes.func,
  onSelectSlot: PropTypes.func,
  replaceEvent: PropTypes.func
};

export default Month;
