import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import styles from "../styles/week_style.module.css";
import WeekEvents from "./WeekEvents";
import moment from "moment";
import WeekContext from "../Context/WeekContext";
import DayViewWeekEvents from "./DayViewWeekEvents";

const TimeRow = props => {
  const {
    time,
    weekArray,
    weekEvents,
    onSelectEvent,
    onSelectSlot,
    replaceEvent,
    sourceEvent,
    setSourceEvent,
    resources
  } = props;
  const [entered, setEntered] = useState({});

  const { data, setData } = useContext(WeekContext);

  const selectSlot = (slot) => {
    let date = slot.format("YYYY-MM-DD");
    let start = date + ` ${time}`;
    let end = date + ` ${moment(time, "HH:mm").add(30, "minute").format("HH:mm")}`;
    let slotInfo = {
      start: moment(start).toDate(),
      end: moment(end).toDate()
    };
    onSelectSlot(slotInfo);
  };

  const onDragEnter = (e, item) => {
    e.preventDefault();
    e.stopPropagation();
    // console.log(item.format("YYYY-MM-DD"))
    setEnteredBoolean(item, true);
    let target = e.currentTarget;
    setData({
      ...data,
      dragEnterPosition: {
        top: target.offsetTop,
        left: target.offsetLeft,
        width: target.offsetWidth,
        position: "absolute",
        zIndex: 10
      },
      draggingEvent: getDraggedEventTime(sourceEvent, item)
    });

  };
  const onDragLeave = (e, item) => {
    e.preventDefault();
    e.stopPropagation();
    setEnteredBoolean(item, false);
  };

  const setEnteredBoolean = (item, bool = false) => {
    let count = entered[[`${item.format("YYYY-MM-DD")}_${time}`]] || 0;
    bool ? count++ : (count > 0 && count--);
    setEntered({
      ...entered,
      [`${item.format("YYYY-MM-DD")}_${time}`]: count
    });
  };

  const onDrop = (e, date, cancel = false) => {
    setEntered({});
    e.preventDefault();
    e.stopPropagation();
    let event = e.dataTransfer.getData("event");
    // let source_event = JSON.parse(event);
    let source_event = JSON.parse(JSON.stringify(sourceEvent));
    let target_event = getDraggedEventTime(source_event, date, cancel);
    // let target_date = date.format("YYYY-MM-DD");
    // let start_time = `${target_date} ${time}`;
    // let target_event = {
    //   ...source_event,
    //   start: start_time,
    //   end: moment(start_time).add(event_diff, "minute").format("YYYY-MM-DD HH:mm")
    // };
    replaceEvent(source_event, target_event);
    e.dataTransfer.clearData();
    setData({ ...data, isDragStarted: false, dragEnterPosition: { display: "none" } });
  };

  const getDraggedEventTime = (source_event, date, cancel = false) => {
    let Time = (cancel ? moment(data.draggingEvent.start).format("HH:mm") : time);
    let target_date = cancel ? moment(data.draggingEvent.start).format("YYYY-MM-DD") : date.format("YYYY-MM-DD");
    let event_diff = moment(source_event.end).diff(moment(source_event.start), "minute");
    let start_time = `${target_date} ${Time}`;
    return {
      ...source_event,
      start: start_time,
      end: moment(start_time).add(event_diff, "minute").format("YYYY-MM-DD HH:mm")
    };
  };

  const onDragOver = (e) => {
    e.preventDefault();
    e.stopPropagation();
  };
  let showTime = ["00"].includes(moment(time, "HH:mm").format("mm"));
  let showBorder = ["15", "45"].includes(moment(time, "HH:mm").format("mm"));
  let formattedTime = moment(time, "hh:mm").format("h A");
  return (
    <tbody>
    <tr>
      <td className={`${styles.time} ${!showBorder ? styles.noBorder : ""}`}>{showTime ? formattedTime : ""}</td>
      {
        weekArray ? weekArray.map((item, index) => {
            let isEntered = entered[`${item.format("YYYY-MM-DD")}_${time}`] > 0;
            return (
              <td
                onDragEnter={(e) => onDragEnter(e, item, index)}
                onDragLeave={(e) => onDragLeave(e, item)}
                onDrop={(e) => onDrop(e, item)}
                onDragOver={onDragOver}
                onDragEnd={(e) => {
                  onDrop(e, item, true);
                  e.target.style.zIndex = 2;
                  e.target.style.opacity = 1;
                }}
                className={(!showBorder ? styles.noBorder : "") + " " + (isEntered ? styles.dropzone : "")}
                onClick={() => selectSlot(item)} style={{ position: "relative" }} key={index}>
                {
                  (resources && resources.length > 1) ?
                    <WeekEvents {...{ weekEvents, onSelectEvent, isEntered, sourceEvent, setSourceEvent }} weekDay={item}
                                time={time} /> :
                    <DayViewWeekEvents {...{ weekEvents, onSelectEvent, isEntered, sourceEvent, setSourceEvent }}
                                       weekDay={item}
                                       time={time} />
                }
              </td>
            );
          })
          : <td></td>
      }
    </tr>
    </tbody>
  );
};

TimeRow.propTypes = {
  time: PropTypes.string,
  weekArray: PropTypes.array,
  weekEvents: PropTypes.object,
  onSelectEvent: PropTypes.func,
  onSelectSlot: PropTypes.func,
  replaceEvent: PropTypes.func,
  sourceEvent: PropTypes.object,
  setSourceEvent: PropTypes.func
};

export default TimeRow;
