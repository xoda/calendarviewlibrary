import React, { useContext } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import styles from "../styles/week_style.module.css";
import Event from "./WeekEvents/Event";
import WeekContext from "../Context/WeekContext";
import DraggingEvent from "./WeekEvents/DraggingEvent";

const DayViewWeekEvents = props => {
  const { weekDay, weekEvents, time, onSelectEvent, isEntered, sourceEvent, setSourceEvent } = props;
  let dateTime = weekDay.format("YYYY-MM-DD") + ` ${time}`;
  let {data,setData}=useContext(WeekContext)
  const onDragStart = (e, item) => {
    let el = e.target.cloneNode();
    el.style.display = "none";
    e.dataTransfer.setDragImage(el, 0, 0);
    e.dataTransfer.setData("event", JSON.stringify(item));
    setSourceEvent(item);
    setData({...data,isDragStarted: true})
    let target = e.target
    setTimeout(() => {
      target.style.zIndex = -1;
      target.style.opacity = 0.7
    },100)
  };
  return (
    <div>
      {
        weekEvents[dateTime] && weekEvents[dateTime].map((item, index) => {
          if (index < 5)
            return (
              <div className={styles.onDrag} >
              <DraggingEvent {...{ item, onDragStart, onSelectEvent }} />
              </div>
            );
        })
      }
      {/*{*/}
      {/*  (isEntered && (!weekEvents[dateTime] || !~weekEvents[dateTime].findIndex(el => el.id === sourceEvent.id))) &&*/}
      {/*  <Event item={sourceEvent} movingEvent={true} />*/}
      {/*}*/}
      {/*{*/}
      {/*  weekEvents[dateTime] && weekEvents[dateTime].length > 5 &&*/}
      {/*  <div*/}
      {/*    className={styles.event}*/}
      {/*  >{weekEvents[dateTime].length - 5}+</div>*/}
      {/*}*/}
    </div>
  );
};

DayViewWeekEvents.propTypes = {
  weekDay: PropTypes.object,
  weekEvents: PropTypes.object,
  time: PropTypes.string,
  onSelectEvent: PropTypes.func,
  sourceEvent: PropTypes.object,
  setSourceEvent: PropTypes.func
};

export default DayViewWeekEvents;
