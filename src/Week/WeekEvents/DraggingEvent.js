import React, { useContext, useState } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import styles from "../../styles/week_style.module.css";
import getEventSpecs from "../../utils/getEventSpecs";
import WeekContext from "../../Context/WeekContext";
const DraggingEvent = props => {
  const { item, onSelectEvent, onDragStart, movingEvent,setSourceEvent } = props;
  let { height, offset,smallEventStyle } = getEventSpecs(item,true);
  let {data} = useContext(WeekContext)
  let extraTextStyle= {}
  let extraEventStyle= {}
  if(movingEvent) {
    extraTextStyle = {...extraTextStyle,fontSize:'100%',fontWeight: "bold"}
    extraEventStyle = {...extraEventStyle,zIndex: 2,pointerEvents: "none", ...data.dragEnterPosition}
  }
  let smallEvent = height <=35
  return (
    <div
      draggable={!movingEvent}
      onDragStart={(e) => onDragStart(e, item)}
      title={item.tooltip || `${item.title}: ${moment(item.start).format("hh:mm A")} -- ${moment(item.end).format("hh:mm A")}`}
      className={styles.event}
      style={{ minHeight: height, top: offset, boxSizing: "border-box", ...{ background: item.background },...extraEventStyle,...smallEventStyle }}
      onClick={(e) => {
        e.stopPropagation();
        onSelectEvent(item);
      }}
    >
                <span
                  style={{ fontSize: "75%" ,...extraTextStyle}}>{moment(item.start).format("hh:mm A")} - {
                  moment(item.end).format("hh:mm A")} {smallEvent ? `- ${item.title}` : ""}</span><br />
      {!smallEvent && item.title}
    </div>
  );
};

DraggingEvent.propTypes = {
  movingEvent: PropTypes.bool,
  onSelectEvent: PropTypes.func,
  onDragStart: PropTypes.func,
  item: PropTypes.object.isRequired
};

export default DraggingEvent;
