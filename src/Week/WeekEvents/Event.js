import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import styles from "../../styles/week_style.module.css";
import  classnames from 'classnames'

const Event = props => {
  const {item,onSelectEvent,onDragStart,movingEvent} = props
  return (
    <div>
      <div
        draggable={true}
        onDragStart={(e) => onDragStart(e, item)}
        title={item.tooltip || `${item.title}: ${moment(item.start).format('hh:mm A')} - ${moment(item.end).format('hh:mm A')}`}
        className={classnames(styles.event,styles.circle)}
        style={item.background ? { background: item.background } : {}}
        onClick={(e) => {
          e.stopPropagation()
          onSelectEvent(item)
        }}
      >{item.avatarText}</div>
    </div>
  );
};

Event.propTypes = {
  onSelectEvent: PropTypes.func,
  onDragStart: PropTypes.func,
  item: PropTypes.object.isRequired,
  movingEvent: PropTypes.bool,
};

export default Event;
