import React, { useState } from "react";
import PropTypes from "prop-types";
import MonthContext from "../Context/MonthContext";

const MonthProvider = props => {
  const [data, setData] = useState({
    dragEnterPosition: {
      display: "none"
    },
    isDragStarted: false,
    draggingEvent: {}
  });
  return (
    <div>
      <MonthContext.Provider value={{ data, setData }}>
        {props.children}
      </MonthContext.Provider>
    </div>
  );
};

MonthProvider.propTypes = {};

export default MonthProvider;
