import React, { useState } from "react";
import PropTypes from "prop-types";
import DayContext from "../Context/DayContext";

const DayProvider = props => {
  const [data, setData] = useState({
    dragEnterPosition: {
      display: "none"
    },
    isDragStarted: false,
    draggingEvent: {}
  });
  return (
    <div>
      <DayContext.Provider value={{ data, setData }}>
        {props.children}
      </DayContext.Provider>
    </div>
  );
};

DayProvider.propTypes = {};

export default DayProvider;
