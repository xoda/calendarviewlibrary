import React, { useState } from "react";
import PropTypes from "prop-types";
import WeekContext from "../Context/WeekContext";

const WeekProvider = props => {
  const [data, setData] = useState({
    dragEnterPosition: {
      display: "none"
    },
    isDragStarted: false,
    draggingEvent: {}
  });
  return (
    <div>
      <WeekContext.Provider value={{ data, setData }}>
        {props.children}
      </WeekContext.Provider>
    </div>
  );
};

WeekProvider.propTypes = {};

export default WeekProvider;
