import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import styles from "../../styles/month_style.module.css";

const Event = props => {
  const {item,onSelectEvent,onDragStart} = props
  return (
      <div
        draggable={true}
        onDragStart={(e) => onDragStart(e, item)}
        title={item.tooltip || `${item.title}: ${moment(item.start).format('hh:mm A')} -- ${moment(item.end).format('hh:mm A')}`}
        className={styles.event}
        style={item.background ? { background: item.background } : {}}
        onClick={(e) => {
          e.stopPropagation()
          onSelectEvent(item)
        }}
      >{item.avatarText}</div>
  );
};

Event.propTypes = {
  onSelectEvent: PropTypes.func,
  onDragStart: PropTypes.func,
  item: PropTypes.object.isRequired,
};

export default Event;
