import React, { useContext } from "react";
import PropTypes from "prop-types";
import styles from "../styles/month_style.module.css";
import moment from "moment";
import Event from "./DayEvents/Event";
import MonthContext from "../Context/MonthContext";

const DayEvents = props => {
  const { dayEvents, onSelectEvent, isEntered, sourceEvent, setSourceEvent } = props;
  const {data,setData} = useContext(MonthContext)
  const onDragStart = (e, item) => {
    let el = e.target.cloneNode();
    el.style.display = "none";
    e.dataTransfer.setDragImage(el, 0, 0);
    e.dataTransfer.setData("event", JSON.stringify(item));
    setSourceEvent(item);
    setData({...data,isDragStarted: true})
  };
// console.log(dayEvents.map(el => el.id))
  return (
    <div style={{ display: "flex", marginTop: 15, position: "absolute", left: 2 }}>
      {
        dayEvents && dayEvents.map((item, index) => {
          if (index < 5)
            return (
              <Event {...{ item, onSelectEvent, onDragStart }} />
            );
        })
      }
      {/*{*/}
      {/*  (isEntered && (!dayEvents || !dayEvents.map(el => el.id).includes(sourceEvent.id))) &&*/}
      {/*  <Event item={sourceEvent} />*/}
      {/*}*/}
      {
        dayEvents && dayEvents.length > 5 &&
        <div
          className={styles.event}
        >{dayEvents.length - 5}+</div>
      }

    </div>
  );
};

DayEvents.propTypes = {
  dayEvents: PropTypes.array,
  onSelectEvent: PropTypes.func,
  sourceEvent: PropTypes.object,
  setSourceEvent: PropTypes.func
};

export default DayEvents;
