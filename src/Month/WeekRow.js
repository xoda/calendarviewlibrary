import React, { useContext, useState } from "react";
import PropTypes from 'prop-types'
import moment from 'moment'
import DayEvents from './DayEvents'
import month_styles from '../styles/month_style.module.css'
import MonthContext from "../Context/MonthContext";

const WeekRow = props => {
  let { days, monthEvents, onSelectEvent, onSelectSlot, date, replaceEvent,sourceEvent,setSourceEvent } = props
  const [entered, setEntered] = useState({})
  const {data,setData} = useContext(MonthContext)
  const selectSlot = (slot) => {
    let datetime = slot.format('YYYY-MM-DD HH:mm')
    let slotInfo = {
      start: moment(datetime).toDate(),
      end: moment(datetime).toDate()
    }
    onSelectSlot(slotInfo)
  }

  const onDragEnter = (e, item) => {
    e.preventDefault()
    e.stopPropagation()
    setEnteredBoolean(item, true)
    let target = e.currentTarget;
    setData({
      ...data, dragEnterPosition: {
        top: target.offsetTop+20,
        left: target.offsetLeft,
        width: target.offsetWidth,
        position: "absolute",
        zIndex: 10
      },
      draggingEvent: sourceEvent
    });
  }
  const onDragLeave = (e, item) => {
    e.preventDefault()
    e.stopPropagation()
    setEnteredBoolean(item, false)
  }

  const setEnteredBoolean = (item, bool = false) => {
    let count = entered[item.format('YYYY-MM-DD')] || 0
    bool ? count++ : (count > 0 && count--)
    setEntered({
      ...entered,
      [item.format('YYYY-MM-DD')]: count
    })
  }

  const onDrop = (e, date) => {
    setEntered({})
    e.preventDefault()
    e.stopPropagation()
    let event = e.dataTransfer.getData('event')
    let source_event = JSON.parse(event)
    let target_date = date.format('YYYY-MM-DD')
    let target_event = {
      ...source_event,
      start: target_date + ` ${moment(source_event.start).format('HH:mm')}`,
      end: target_date + ` ${moment(source_event.end).format('HH:mm')}`
    }
    replaceEvent(source_event, target_event)
    e.dataTransfer.clearData()
    setData({...data,isDragStarted: false,dragEnterPosition: {display: "none"}})
  }

  const onDragOver = (e) => {
    e.preventDefault()
    e.stopPropagation()
  }
  return (
    <tr>
      {
        days && days.map((item, index) => {
          let dayEvents = monthEvents[item.format('YYYY-MM-DD')]
          let isCurrentMonth = moment(date).format('MM') === item.format('MM')
          let isEntered = entered[item.format('YYYY-MM-DD')] > 0
          return (
            <td
              onDragEnter={(e) => onDragEnter(e, item)}
              onDragLeave={(e) => onDragLeave(e, item)}
              onDrop={(e) => onDrop(e, item)}
              onDragEnd={() => {
                setData({...data,isDragStarted: false,dragEnterPosition: {display: "none"}})
              }}
              onDragOver={onDragOver}
              className={(!isCurrentMonth ? month_styles.disabled : '') + ' ' + (isEntered ? month_styles.dropzone : '')}
              onClick={(e) => selectSlot(item)}
              style={{ position: 'relative' }} key={index}>
              <div style={{ textAlign: 'right', pointerEvents: 'none' }}>
                {isCurrentMonth && item.format('DD')}
              </div>
              <DayEvents {...{ dayEvents, onSelectEvent,isEntered,sourceEvent,setSourceEvent }} />
            </td>
          )
        })
      }
    </tr>
  )
}

WeekRow.propTypes = {
  days: PropTypes.array,
  monthEvents: PropTypes.object,
  onSelectEvent: PropTypes.func,
  onSelectSlot: PropTypes.func,
  replaceEvent: PropTypes.func,
  sourceEvent: PropTypes.object,
  setSourceEvent: PropTypes.func
}

export default WeekRow
