import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Month from "./Month";
import Day from "./Day";
import ToolBar from "./ToolBar";
import Week from "./Week";
import style from "./styles/style.module.css";
import classNames from "classnames";
import usePrevious from "./usePrevious";
import arrayEquals from "./utils/arrayEquals";
import DayProvider from "./Provider/DayProvider";
import WeekProvider from "./Provider/WeekProvider";
import MonthProvider from "./Provider/MonthProvider";

const ReactCalendarSchedule = (props) => {
  const {
    rootClassName,
    components,
    onDropEvent,
    events: eventsFromProps,
    view: viewFromProps,
    date: dateFromProps
  } = props;
  const [view, onViewChange] = useState(viewFromProps);
  const [date, onNavigate] = useState(dateFromProps);
  const [events, setEvents] = useState(eventsFromProps);
  const CustomToolbar = components && components.Toolbar;

  let previousData = usePrevious({
    eventsFromProps,
    viewFromProps,
    dateFromProps
  });
  // component did update
  useEffect(() => {
    if (previousData) {
      if (!arrayEquals(previousData.eventsFromProps, eventsFromProps))
        setEvents(eventsFromProps);
      if (previousData.viewFromProps !== viewFromProps)
        onViewChange(viewFromProps);
      if (previousData.dateFromProps !== dateFromProps)
        onNavigate(dateFromProps);
    }

  }, [eventsFromProps, dateFromProps, viewFromProps]);

  const resetEvents = () => {
    setEvents(props.events);
  };

  const replaceEvent = (source_event, target_event) => {
    let tempEvents = JSON.parse(JSON.stringify(events));
    let index = tempEvents.findIndex(el => el.id === source_event.id);
    if (~index) {
      tempEvents[index] = target_event;
      setEvents(tempEvents);
      onDropEvent({
        source: source_event,
        target: target_event,
        resetEvents
      });
    }
  };

  const getView = () => {
    switch (view) {
      case "month":
        return (<MonthProvider><Month {...props} {...{ date, events, replaceEvent }} /></MonthProvider>);
      case "day":
        return (<DayProvider><Day {...props} {...{ date, events, replaceEvent }} /></DayProvider>);
      case "week":
        return (<WeekProvider><Week {...props} {...{ date, events, replaceEvent }} /></WeekProvider>);
      default:
        return (<MonthProvider><Month {...props} {...{ date, events, replaceEvent }} /></MonthProvider>);
    }
  };
  return (
    <div className={(classNames(style.root, rootClassName))}>
      {CustomToolbar ? <CustomToolbar {...{ onViewChange, onNavigate, view, date }} /> :
        <ToolBar {...{ onViewChange, onNavigate }} />}
      <div className={style.calendar}>
        {getView()}
      </div>
    </div>
  );
};

ReactCalendarSchedule.propTypes = {
  view: PropTypes.string,
  date: PropTypes.string,
  events: PropTypes.array.isRequired,
  resources: PropTypes.array,
  resourceTitleAccessor: PropTypes.string,
  resourceIdAccessor: PropTypes.string,
  fixedHeader: PropTypes.bool,
  onSelectEvent: PropTypes.func,
  onSelectSlot: PropTypes.func,
  rootClassName: PropTypes.string,
  monthRootClassName: PropTypes.string,
  weekRootClassName: PropTypes.string,
  dayRootClassName: PropTypes.string,
  components: PropTypes.object,
  onDropEvent: PropTypes.func
};

export default ReactCalendarSchedule;
